CREATE OR REPLACE PACKAGE BODY OWUSER.LOP_RO_LCWON
AS
/*******************************************************************************
**
** Procedure     : MAIN_12H
**
** Description   : Execute every 12H
** Input         : -
**
*******************************************************************************/
PROCEDURE MAIN_12H
IS
BEGIN
    DELETE_PP_LCWON;
    CREATE_PP_FIKTIV;
    UPDATE_STORAGE_RULE;
    UPDATE_PAK;
END MAIN_12H;
/*******************************************************************************
**
** Procedure     : MAIN_15MIN
**
** Description   : Execute every 15MIN
** Input         : -
**
*******************************************************************************/
PROCEDURE MAIN_15MIN
IS
BEGIN
    CREATE_PP_FIKTIV;
    UPDATE_STORAGE_RULE;
    UPDATE_PAK;
    PRIO_PICK_ORDER('BUK1', 'RO-LCWON', 2, 'WPE1');
END MAIN_15MIN;
/*******************************************************************************
**
** Procedure     : CREATE_PP_FIKTIV.
**
** Description   : Create virtual picklocation for RO-LCWOFF products
** Input         : -
** REVISIONS:
** 
** Ver 		Date      		Player       SN/JIRA 		Description
** 
** 1.0 		??.??.?? 		????????            	    created
** 1.1 		19.04.2022 		DAKORNHA     IMI-1437       adding one liners pick location / max. 500 rows at once
** 1.2 		19.04.2022 		DAKORNHA     IMI-1437       adopting SQL for better performance
** 1.3 		29.04.2022 		DAKORNHA     INC005468881   pick locations will only when there is stock and for existing customer orders first. removed check on RCVROW.
** 1.4 		05.05.2022 		DAKORNHA                    refactoring 
**
*******************************************************************************/
PROCEDURE CREATE_PP_FIKTIV
IS
   WHID_W            PP.WHID%type;
   WSID_W            PP.WSID%type;
   WPADR_W           PP.WPADR%type;
   OWNID_W           PP.OWNID%type;
   ARTID_W           PP.ARTID%type;
   COMPANY_ID_W      COMPANY.COMPANY_ID%type;
   PPVERIFY_W        PP.PPVERIFY%type;
   PIKSTAT_W         PP.PIKSTAT%type;
   RFLSTAT_W         PP.RFLSTAT%type;
   BUF_RFLLVL_W      PP.BUF_RFLLVL%type;
   AUTOADD_W         PP.AUTOADD%type;
   RFLLVL_W          PP.RFLLVL%type;
   REPL_FROM_RCV_W   PP.REPL_FROM_RCV%type;
   ITETRACE_W        PP.ITETRACE%type;
   ITEVERIFY_W       PP.ITEVERIFY%type;
   RO_QTYCALC_W      PP.RO_QTYCALC%type;
   MINRFLQTY_W       PP.MINRFLQTY%type;
   MAXRFLQTY_W       PP.MAXRFLQTY%type;
   RECRFLQTY_W       PP.RECRFLQTY%type;
   PZID_W            PPPZ.PZID%type;
   PIKPAKID_W        PPPZ.PIKPAKID%type;
   MINPIKPAKQTY_W    PPPZ.MINPIKPAKQTY%type;
   MAXPIKPAKQTY_W    PPPZ.MINPIKPAKQTY%type;
   PICKSEQ_W         PPPZ.PICKSEQ%type;
   CREATEEMPID_W     PP.CREATEEMPID%type;
   BASPAKID_W        PP.BASPAKID%type;
   SUPID_W           RCVHEAD.SUPID%type;
   ALMID_O           VARCHAR2(35);
   ARTTYPID_W        ART.ARTTYPID%type;
   ARTGROUP_W        ART.ARTGROUP%type;
   BASEQTY_W         PAK.BASEQTY%type;
   BASEQTY_CAR       PAK.BASEQTY%type;
   BASEQTY_PCE       PAK.BASEQTY%type;
   BASPACK_CAR       PAK.BASPAKID%type;
   BASPACK_PCE       PAK.BASPAKID%type;
   PPKEY_E           PP.PPKEY%type;
   PPKEY_W           PP.PPKEY%type;
   PAKID_W           PAK.PAKID%type;
   STORZONE_W        PPBUF.STORZONE%type;
   STORDIST_W        PPBUF.STORDIST%type;
   MINZLVL_W         PPBUF.MINZLVL%type;
   MAXZLVL_W         PPBUF.MAXZLVL%type;
   SEQNUM_W          PPBUF.SEQNUM%type;
   BUFWSID_W         PPBUF.BUFWSID%type;      
    
   
    
    BEGIN
        
    FOR PP IN (
        SELECT WP.WHID AS WHID_W
            , WP.WSID AS WSID_W
            , WP.WPADR AS WPADR_W
            , nvl(ART.OWNID, ART.COMPANY_ID) AS OWNID_W
            , ART.ARTID AS ARTID_W
            , ART.COMPANY_ID AS COMPANY_ID_W
            , 'A' AS PPVERIFY_W
            , 1 AS PIKSTAT_W
            , 1 AS RFLSTAT_W
            , null AS BUF_RFLLVL_W  
            , 1 AS AUTOADD_W
            , null AS RFLLVL_W
            , 'N' AS REPL_FROM_RCV_W
            , 1 AS ITETRACE_W
            , 0 AS ITEVERIFY_W
            , 'R' AS RO_QTYCALC_W
            , 0 AS MINRFLQTY_W
            , null AS MAXRFLQTY_W
            , null AS RECRFLQTY_W
            , WP.PZID AS PZID_W 
            , ART.BASPAKID AS PIKPAKID_W
            , 1 AS MINPIKPAKQTY_W
            , null AS MAXPIKPAKQTY_W
            , DECODE(WP.WPADR, 'PICK_E1', 1 
                            , 'PICK_E2', 2
                            , 'PICK_E1_OL', 3
                            , 'PICK_E2_OL', 4
                            , 'PICK_OFF_WH', 5
                            , 'PICK_OFF_WH2', 6
                            , 0) AS PICKSEQ_W
            , 'CREATE_PP_FIKTIV' AS CREATEEMPID_W
            , null AS PPCOD_I                
            , null AS PROMOTN_I              
            , null AS STORBAT_I              
            , ART.BASPAKID AS BASPAKID_W             
            , null AS ALLOWED_STDISCQTY_I
            , 0 AS CRFLLVL_I 
            , DECODE(WP.PZID, 'WPE1', 1 
                            , 'WPE2', 2
                            , 'WPE1_OL', 3
                            , 'WPE2_OL', 4
                            , 'WPE_OFF_WH', 9
                            , 0) AS PRIORITY   
            --, ALMID_O               
            , 0 AS WWS_PICK_FOR_REPL_I
            , COROW.ARTID
                    FROM ART
        JOIN ITE
            ON ITE.ARTID = ART.ARTID
            AND ITE.COMPANY_ID = ART.COMPANY_ID
        JOIN WP
            ON WP.WHID = ITE.WHID
        LEFT JOIN PP
            ON PP.ARTID = ART.ARTID
            AND PP.COMPANY_ID = ART.COMPANY_ID
            AND PP.WPADR = WP.WPADR
            AND PP.WHID = 'BUK1'
            AND PP.WSID IN ('WE1', 'WE2')
        LEFT JOIN COROW
            ON COROW.ARTID = ART.ARTID
            AND COROW.COMPANY_ID = ART.COMPANY_ID
            AND ART.COMPANY_ID = 'RO-LCWON'
            AND COROW.COROWSTAT IN ('0', 'E')
        WHERE ART.COMPANY_ID = 'RO-LCWON'
            AND ART.OWNID IS NULL
            AND WP.WHID = 'BUK1'
            AND WP.WPADR LIKE 'PICK%'
            AND WP.WPTYPE = 'P'
            AND WP.WSID IN ('WE1', 'WE2')
            AND PP.PPKEY IS NULL
        GROUP BY WP.WHID
            , WP.WSID 
            , WP.WPADR 
            , NVL(ART.OWNID, ART.COMPANY_ID) 
            , ART.ARTID 
            , ART.COMPANY_ID 
            , WP.PZID 
            , ART.BASPAKID 
            , DECODE(WP.WPADR, 'PICK_E1', 1 
                            , 'PICK_E2', 2
                            , 'PICK_E1_OL', 2
                            , 'PICK_E2_OL', 2
                            , 'PICK_OFF_WH', 5
                            , 'PICK_OFF_WH2', 6
                            , 0)         
            , ART.BASPAKID   
            , DECODE(WP.PZID, 'WPE1', 1 
                            , 'WPE2', 2
                            , 'WPE1_OL', 3
                            , 'WPE2_OL', 4
                            , 'WPE_OFF_WH', 9
                            , 0)  
            , COROW.ARTID
        ORDER BY 
            COROW.ARTID NULLS LAST,
            ART.ARTID,
            WP.WSID,
            WP.WPADR
        )
    LOOP
 
        PICK_PLACE.New(PP.WHID_W
                    ,PP.WSID_W
                    ,PP.WPADR_W
                    ,PP.OWNID_W
                    ,PP.ARTID_W
                    ,PP.COMPANY_ID_W
                    ,PP.PPVERIFY_W
                    ,PP.PIKSTAT_W
                    ,PP.RFLSTAT_W
                    ,PP.BUF_RFLLVL_W
                    ,PP.AUTOADD_W
                    ,PP.RFLLVL_W
                    ,PP.REPL_FROM_RCV_W   
                    ,PP.ITETRACE_W
                    ,PP.ITEVERIFY_W
                    ,PP.RO_QTYCALC_W
                    ,PP.MINRFLQTY_W
                    ,PP.MAXRFLQTY_W
                    ,PP.RECRFLQTY_W
                    ,PP.PZID_W 
                    ,PP.PIKPAKID_W
                    ,PP.MINPIKPAKQTY_W
                    ,PP.MAXPIKPAKQTY_W
                    ,PP.PICKSEQ_W
                    ,PP.CREATEEMPID_W
                    ,PP.PPCOD_I    -- PPCOD_I                
                    ,PP.PROMOTN_I    -- PROMOTN_I              
                    ,PP.STORBAT_I    -- STORBAT_I              
                    ,PP.BASPAKID_W             
                    ,PP.ALLOWED_STDISCQTY_I    -- ALLOWED_STDISCQTY_I
                    ,PP.CRFLLVL_I       -- CRFLLVL_I 
                    ,PP.PRIORITY                          --PRIORITY
                    ,ALMID_O                    
                    ,PP.WWS_PICK_FOR_REPL_I);   --WWS_PICK_FOR_REPL_I);
        
        COMMIT;
    END LOOP;

END CREATE_PP_FIKTIV;

/*******************************************************************************
**
** Procedure     : UPDATE_STORAGE_RULE.
**
** Description   : Update storage rule on ART and ARTFRCWS 
** Input         : -
**
*******************************************************************************/
PROCEDURE UPDATE_STORAGE_RULE
IS
BEGIN
    --ART
    --change Storage Rule to "Area"
    UPDATE ART SET STORRULE = 'WS'
    WHERE ART.COMPANY_ID = 'RO-LCWON'
        AND ART.OWNID IS NULL
        AND ART.STORRULE != 'WS'
        AND (ART.ARTID IN
                (SELECT ARTID 
                    FROM ITE
                    WHERE COMPANY_ID = 'RO-LCWON')
         OR ART.ARTID IN
                (SELECT ARTID 
                    FROM RCVROW
                    WHERE COMPANY_ID = 'RO-LCWON'
                        AND WHID = 'BUK1'));
    
    --ARTFRCWS 
    --add Warehouse Product Storage Rule
    INSERT INTO ARTFRCWS
    SELECT ART.ARTID,
        WS.WHID,
        CASE WS.WSID 
            WHEN 'WE1' THEN 1
            WHEN 'WE2' THEN 2
        END AS FRCSEQ,
        WS.WSID,
        NULL,
        0,
        99,
        COMPANYWH.COMPANY_ID,
        'UPDATE_STORAGE_RULE',
        SYSDATE
    FROM ART
    JOIN COMPANYWH 
        ON ART.COMPANY_ID = COMPANYWH.COMPANY_ID
    JOIN ARTTYP
        ON ARTTYP.ARTTYPID = ART.ARTTYPID
        AND ARTTYP.COMPANY_ID = ART.COMPANY_ID
    JOIN WS 
        ON WS.WHID = COMPANYWH.WHID       
    WHERE WS.WSID IN ('WE1', 'WE2') 
        AND COMPANYWH.WHID = 'BUK1'
        AND COMPANYWH.COMPANY_ID = 'RO-LCWON'
        AND NOT EXISTS
        (SELECT 'X'
            FROM ARTFRCWS
            WHERE WHID = COMPANYWH.WHID
                AND FRCWSID = WS.WSID
                AND ARTID = ART.ARTID
                AND COMPANY_ID = ART.COMPANY_ID)            
        AND (EXISTS
                (SELECT 'X' 
                    FROM ITE
                    WHERE COMPANY_ID = ART.COMPANY_ID
                        AND ARTID = ART.ARTID)
         OR EXISTS
                (SELECT 'X' 
                    FROM RCVROW
                    WHERE COMPANY_ID = ART.COMPANY_ID
                        AND WHID = COMPANYWH.WHID
                        AND ARTID = ART.ARTID));
    COMMIT;
    
END UPDATE_STORAGE_RULE;

/*******************************************************************************
**
** Procedure     : UPDATE_PAK.
**
** Description   : Update PAK
**                  * Pieces:
**                  ** PALPAK = 1
**                  ** CARTYPID = 'WSP'
** Input         : -
**
*******************************************************************************/
PROCEDURE UPDATE_PAK
IS
BEGIN
    UPDATE PAK SET PALPAK = 1, CARTYPID = 'WSP'
    WHERE PAK.COMPANY_ID = 'RO-LCWON'
        AND BASEQTY = 1
        AND (PALPAK = 0
         OR CARTYPID is null)
        AND (PAK.ARTID IN
                (SELECT ARTID 
                    FROM ITE
                    WHERE COMPANY_ID = 'RO-LCWON')
         OR PAK.ARTID IN
                (SELECT ARTID 
                    FROM RCVROW
                    WHERE COMPANY_ID = 'RO-LCWON'
                        AND WHID = 'BUK1'));
    COMMIT;
END UPDATE_PAK;

/*******************************************************************************
**
** Procedure     : DELETE_PP_LCWON
**
** Description   : Deletes all PP where no stock exists and where the upddtm is older 60 days
** Input         : -
**
*******************************************************************************/

PROCEDURE DELETE_PP_LCWON
IS
BEGIN
DELETE 
            FROM PP
 WHERE     PP.WHID = 'BUK1'
       AND upddtm < SYSDATE - 60
       AND PP.COMPANY_ID = 'RO-LCWON'
       AND PP.ARTID NOT IN (SELECT DISTINCT ARTID
                              FROM (SELECT artid
                                      FROM ITE
                                     WHERE COMPANY_ID = 'RO-LCWON'
                                    UNION ALL
                                    SELECT artid
                                      FROM rcvrow
                                     WHERE company_id = 'RO-LCWON'))
                                     and rownum <=2000;
           commit;                        
END DELETE_PP_LCWON;

/*******************************************************************************
**
** Procedure     : PRIO_PICK_ORDER
**
** Description   : Change prio for pick orders who contain same customer order a different pick zone
** Input         : WHID_I = Warehous Identity
**                 COMPNANY_ID_I = Company Identity
**                 PRI_I = new pick order priority
**                 PZID_I = excluded Pick Zone Identity. Use a separator for multiple excluded PZ
**
*******************************************************************************/

PROCEDURE PRIO_PICK_ORDER (
    WHID_I          PBHEAD.WHID%type, 
    COMPANY_ID_I    PBROW.COMPANY_ID%type,
    PRI_I           PBHEAD.PRI%type,
    PZID_I          PBHEAD.PZID%type 
)
IS

    PBHEADID_W      PBHEAD.PBHEADID%type;
    PZID_W          PBHEAD.PZID%type;
    
    CURSOR PICK_ORDER 
            (WHID_C         IN PBHEAD.WHID%type, 
            COMPANY_ID_C    IN PBROW.COMPANY_ID%type,
            PRI_C           IN PBHEAD.PRI%type) 
        IS 
            SELECT PBHEADID,
                PZID
            FROM (
                SELECT PBHEAD.PBHEADID,
                        PBROW.PZID,
                        PBROW.COID,
                        PBROW.COSEQ,
                        PBROW.COSUBSEQ,
                        PBHEAD.PRI,
                        COUNT (DISTINCT PBROW.PZID)
                            OVER (PARTITION BY PBROW.COID, PBROW.COSEQ, PBROW.COSUBSEQ)    ZONES
                    FROM PBHEAD JOIN PBROW ON PBROW.PBHEADID = PBHEAD.PBHEADID
                    WHERE PBHEAD.WHID = WHID_C
                        AND PBROW.COMPANY_ID = COMPANY_ID_C
                GROUP BY PBHEAD.PBHEADID,
                        PBROW.PZID,
                        PBROW.COID,
                        PBROW.COSEQ,
                        PBROW.COSUBSEQ,
                        PBHEAD.PRI
                ORDER BY PBROW.COID, 
                        PBROW.COSEQ, 
                        PBROW.COSUBSEQ)
            WHERE ZONES > 1
                AND PRI > PRI_C;
        
BEGIN

    OPEN PICK_ORDER (WHID_I, COMPANY_ID_I, PRI_I);
    LOOP
        FETCH PICK_ORDER INTO PBHEADID_W, PZID_W;
        EXIT WHEN PICK_ORDER%NOTFOUND;
        
        --skip excluded pick zones
        IF INSTR(PZID_I, PZID_W) > 0 THEN
            CONTINUE;
        END IF;
        
        PICKORDERHEAD.Modify_PRI(PBHEADID_W, PRI_I);
        
    END LOOP;
    CLOSE PICK_ORDER;


END PRIO_PICK_ORDER;


END LOP_RO_LCWON;
/