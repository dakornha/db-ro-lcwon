CREATE OR REPLACE PACKAGE OWUSER.LOP_RO_LCWON AS
PROCEDURE MAIN_12H;
PROCEDURE MAIN_15MIN;
PROCEDURE CREATE_PP_FIKTIV;
PROCEDURE UPDATE_STORAGE_RULE;
PROCEDURE UPDATE_PAK;
PROCEDURE DELETE_PP_LCWON;
PROCEDURE PRIO_PICK_ORDER(WHID_I PBHEAD.WHID%type, COMPANY_ID_I PBROW.COMPANY_ID%type, PRI_I PBHEAD.PRI%type, PZID_I PBHEAD.PZID%type);
END;
/
